// ==UserScript==
// @name         Chamilo Exctract quiz students results Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Construct a csv file to summarize quiz results
// @author       Sébastien Viardot
// @match        https://*/main/exercise/exercise_report.php?*
// @grant        none
// @require   http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @require   https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.6/xlsx.full.min.js
// https://bundle.run/xlsx@0.15.4?name=XLSX
// https://unpkg.com/xlsx@0.15.4/dist/shim.min.js
//  https://unpkg.com/xlsx@0.15.4/dist/xlsx.full.min.js
// ==/UserScript==
const urlBase=document.URL.match(/https\:\/\/[^\/]+/)[0]
const reglesCalcul={"Association":"Si bien associé 1 sinon 0","Libre":"doit être saisi","TexteATrou":"Si bonne valeur 1 sinon 0","ChoixMultiple":"Si bonne valeur 1 sinon -1","ChoixUnique":"Total des point si tout juste sinon 0"}
function nettoie_response(str){
    return str.replace(/(@import.*\);)/,"").replace(/hljs.*$/,"").trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "")
}
function extract_score(score_brut){
    let e=score_brut.match(/([0-9.]+)\s\/\s([0-9.]+)/)
    let score=-1
    let bareme=1
    if (e) {
        score=parseFloat(e[1])
        bareme=parseFloat(e[2])
    }
    return score/bareme
}
function extract_bareme(score_brut){
    let e=score_brut.match(/([0-9.]+)\s\/\s([0-9.]+)/)
    let score=-1
    let bareme=1
    if (e) {
        score=parseFloat(e[1])
        bareme=parseFloat(e[2])
    }
    return bareme
}
function qcm_parse(question,i, q,cadre){
    q.find("tr:nth-child(n+2)").each(
        (j,r) => {
            question[i][j]={
                proposition: nettoie_response($(r).find("td:nth-child(3)").text()),
                repondu: $(r).find("td:nth-child(1) img").attr("title").indexOf("on")>0,
                attendu: $(r).find("td:nth-child(2) img").attr("title").indexOf("on")>0
            }
            if ($(r).find("td:nth-child(2) img").attr("title").indexOf("radio")>=0) question[i]["type"]="ChoixUnique"
            if ($(r).find("td:nth-child(2) img").attr("title").indexOf("check")>=0) question[i]["type"]="ChoixMultiple"
            cadre[i]["attendu"][j]={proposition: question[i][j]["proposition"], attendu: question[i][j]["attendu"] }
        }
    )
}
function association_parse(question,i,q,cadre){
    q.find("tr:nth-child(n+2)").each(
        (j,r) => {
            question[i][j]={"proposition":nettoie_response($(r).find("td:nth-child(1)").text())}
            question[i][j]["repondu"]=nettoie_response($(r).find("td:nth-child(2) span:nth-child(1)").text())
            if ($(r).find("td:nth-child(2) span").length>1){
                question[i][j]["attendu"]=nettoie_response($(r).find("td:nth-child(2) span:last-child").text())
            } else {
                question[i][j]["attendu"]= question[i][j]["repondu"]
            }
            cadre[i]["attendu"][j]={proposition: question[i][j]["proposition"], attendu: question[i][j]["attendu"] }
        }
    )
}
function texte_a_trou_parse(question,i,q,cadre){
    let trous=q.find("tr:nth-child(2) span[style^='border:1px']")
    if (trous.length>0){
        trous.each(
            (j,r) => {
                question[i][j]={"repondu":nettoie_response($(r).find("span:nth-child(1)").text()),
                    "attendu":nettoie_response($(r).find("span:nth-child(3)").text())}
                cadre[i]["attendu"][j]={proposition:"", attendu: question[i][j]["attendu"] }
            }
        )
    } else {
        cadre[i]["attendu"][0]={proposition:"", attendu: "?" }
        question[i]["type"]="Libre"
        question[i][0]={"repondu":nettoie_response(q.find("tr:nth-child(2) td").text())}
    }
}
function extract_response_student(page,cadre){
    let question={}
    let x=page.find(".question_row").each(
        (i,q) => {
            q=$(q)
            let elt=q.find(".title-score .score_exercise")
            if (elt.length>0){
                question["score"]=extract_score(elt.text())
            }else {
                let idq=q.find("a[id^='showfck_fckdiv']").attr("id").match(/showfck_fckdiv(.*)/)[1]
                cadre[idq]={attendu:{},bareme: extract_bareme(q.find(".ribbon .score_exercise").text())}
                question[idq]={intitule: q.find(".page-header h4").text().normalize("NFD").replace(/[\u0300-\u036f]/g, "")}
                question[idq]["score"]=extract_score(q.find(".ribbon .score_exercise").text())
                let nb_colonnes=q.find("tr th").length

                if (nb_colonnes==4) {
                    question[idq]["type"]="Choix"
                    qcm_parse(question, idq, q,cadre)
                }
                if (nb_colonnes==2) {
                    question[idq]["type"]="Association"
                    association_parse(question, idq, q,cadre)
                }
                if (nb_colonnes==1) {
                    question[idq]["type"]="TexteATrou"
                    texte_a_trou_parse(question, idq, q,cadre)
                }
                cadre[idq]["intitule"]=question[idq]["intitule"]
                cadre[idq]["type"]=question[idq]["type"]
            }
        }
    )
    return question
}

function genere_xls(cadre,eleves,bareme){
    let data=[]
    let titre="quiz"
    let merged=[]
    // Construit les premières lignes (entêtes)
    let entete2=["","","","Max Chamilo","Max Excel","Note sur"]
    let entete3=["","","",bareme,"",20]
    let entete1=["","","","","",""]
    let entete4=["login","nom","prenom","Score Chamilo","Score excel","Note élève"]
    let qn=entete4.length
    let formule=""
    let formules = {}
    // Pour chaque question on ajoute les colonnes
    for (const q in cadre){
        let nb_attendus = Object.keys(cadre[q]["attendu"]).length
        //let detail_score=[]
        // Par proposition
        for (let i=0;i<nb_attendus;i++){
            // Première ligne on ajoute le type de question de Chamilo (sur le première colonne) et des "" après (qu'il restera à fusionner)
            entete1.push((i==0)?`${q} - ${cadre[q]["type"]}(${reglesCalcul[cadre[q]["type"]]})`:"");entete1.push("")
            // Deuxième ligne on ajoute l'intitulé de la question sur la premiere colonne et des "" après (qu'il restera à fusionner)
            entete2.push((i==0)?cadre[q]["intitule"]:"");entete2.push("")
            // Troisième ligne pour chaque question on met la proposition (vide???) et un poids par défaut de 1 (pour tous)
            entete3.push(cadre[q]["attendu"][i]["proposition"]);entete3.push(1)
            // Quatrième ligne pour chaque question on met la valeur attendue et soit 0 soit -1 (si c'est un choix multiple)
            entete4.push(cadre[q]["attendu"][i]["attendu"]);entete4.push((cadre[q]["type"]=="ChoixMultiple")?-1:0)
            //detail_score.push(`${XLSX.utils.encode_col(entete3.length-1)}$3`)
        }
        // Complète les lignes 1 et 2 de 2 cases vides
        entete1.push("");entete1.push("")
        entete2.push("");entete2.push("")
        // Complète la ligne 3 de 2 cases pour indiquer le poids de la question récupéré de Chamilo
        entete3.push("Poids Question ->")
        entete3.push(cadre[q]["bareme"])
        // Construit la formule permettant de calculer la somme des poids des questions.
        formule+=(formule?"+":"")+`${XLSX.utils.encode_col(entete3.length-1)}${XLSX.utils.encode_row(2)}`
//        formules[`${XLSX.utils.encode_col(entete3.length-1)}3`]=detail_score.join("+")
        // Ajoute Intutilés Score Chamilo et Score Excel sur la ligne 4
        entete4.push("Score Chamilo");entete4.push("Score Excel")
        // Fusionne les colonnes des lignes 1 et 2
        merged.push({s:{r:0, c:qn},e:{r:0,c:(entete4.length-1)}});
        merged.push({s:{r:1, c:qn},e:{r:1,c:(entete4.length-1)}});
        qn=entete4.length
    }
    // Ajoute dans la structure les lignes d'entêtes complétées.
    data.push(entete1)
    data.push(entete2)
    data.push(entete3)
    data.push(entete4)
    // Ajoute la formule qui calcule la somme des poies des questionsxs
    formules[`${XLSX.utils.encode_col(4)}${XLSX.utils.encode_row(2)}`]=formule
    // Pour chaque élèves on ajoute la ligne correspondante
    for (const e in eleves){
        for (let i=0;i<eleves[e].responses.length;i++){
            formule=""
            // On commence la ligne par
            // login (e) | nom | prenom | le score récupéré de Chamilo | et on fait de la place pour les 2 formules permettant de calculer la note de l'élève via Excel
            let row=[e, eleves[e]["nom"],eleves[e]["prenom"],eleves[e]["responses"][i]["score"]*bareme,"",""]
            // Pour chaque question on ajoute les colonnes.
            for (const q in cadre){
                let nb_attendus = Object.keys(cadre[q]["attendu"]).length
                let detail_score=[]
                let somme_poids=[]
                for (let r=0;r<nb_attendus;r++){
                    // On met en premier la réponse de l'élève
                    row.push(eleves[e]["responses"][i][q][r]["repondu"])
                    // On réserve de la place pour la formule.
                    row.push("xxx")
                    // On mémorise la formule
                    // Version avec la valeur fonction du barème de la ligne 3
                    //formules[`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`]=`IF(${XLSX.utils.encode_col(row.length-2)}${XLSX.utils.encode_row(data.length)}=${XLSX.utils.encode_col(row.length-2)}$4,${XLSX.utils.encode_col(row.length-1)}$3,${XLSX.utils.encode_col(row.length-1)}$4)`
                    // Version avec 1 ou 0 (ou -1) suivant que la réponse est juste ou non, le calcul en fonction du barème se faisant après.
                    formules[`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`]=`IF(${XLSX.utils.encode_col(row.length-2)}${XLSX.utils.encode_row(data.length)}=${XLSX.utils.encode_col(row.length-2)}$4,1,${(cadre[q]["type"]=="ChoixMultiple")?-1:0})`
                    // En vue de construire le score de la question, on mémorise l'identifiant de la case stockant le résultat
                    // Version avec la valeur fonction du barème de la ligne 3 (simple sommme puisque le barème est déjà pris en compte dans la case (mais plus difficile à paramètrer ensuite si on saisi à la main.
                    // detail_score.push(`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`)
                    detail_score.push(`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}*${XLSX.utils.encode_col(row.length-1)}$3`)
                    somme_poids.push(`${XLSX.utils.encode_col(row.length-1)}$3`)
                }
                row.push(eleves[e]["responses"][i][q]["score"]*cadre[q]["bareme"])
                row.push("yyy")
                formule+=(formule?"+":"")+`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`
                if (cadre[q]["type"]=="ChoixUnique") {
                    formules[`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`]=`IF(1=(${detail_score.join("+")})/(${somme_poids.join("+")}),${XLSX.utils.encode_col(row.length-1)}$3,0)`
                } else {
                    formules[`${XLSX.utils.encode_col(row.length-1)}${XLSX.utils.encode_row(data.length)}`]=`max(0,(${detail_score.join("+")})/(${somme_poids.join("+")})*${XLSX.utils.encode_col(row.length-1)}$3)`
                }
            }
            formules[`${XLSX.utils.encode_col(4)}${XLSX.utils.encode_row(data.length)}`]=formule
            formules[`${XLSX.utils.encode_col(5)}${XLSX.utils.encode_row(data.length)}`]=`${XLSX.utils.encode_col(4)}${XLSX.utils.encode_row(data.length)}/${XLSX.utils.encode_col(4)}$${XLSX.utils.encode_row(2)}*${XLSX.utils.encode_col(5)}$${XLSX.utils.encode_row(2)}`
            data.push(row)
        }
    }
    let wb=XLSX.utils.book_new()
    let ws=XLSX.utils.aoa_to_sheet(data)
    for (let cell in formules){
        ws[cell]={f:formules[cell], z: "0.0"}
    }
    ws["!merges"] = merged
    ws["!autofilter"]={ref:`A4:${XLSX.utils.encode_col(entete4.length)}1000`}
    XLSX.utils.book_append_sheet(wb,ws,"Synthese")
    XLSX.writeFile(wb,`quiz_${document.URL.match(/cidReq=([^&]*)/)[1]}.xlsx`)
}


function genere_csv() {
    $("#gensynth").text("Wait")
    $("#gensynth").unbind("click")
    let eleves = {};
    let cadre={}
    let exercise_id = document.URL.match(/exerciseId=([0-9]+)/)[1]
    let url = `${urlBase}/main/inc/ajax/model.ajax.php?a=get_exercise_results&exerciseId=${exercise_id}&filter_by_user=&_search=false&rows=10000&page=1&sidx=&sord=asc`
    $.get(url, res => {
        if (res.rows != undefined) {
            let nb_rows = 0
            let cadre={}
            res.rows.forEach(resultat => {
                let nom = resultat.cell[1]
                let prenom = resultat.cell[0]
                let login = resultat.cell[2]
                let duree = parseFloat(resultat.cell[4])
                let debut = resultat.cell[5]
                let fin = resultat.cell[6]
                let score_brut = resultat.cell[7]
                let extract_score = score_brut.match(/\(([0-9.]+)\s\/\s([0-9.]+)\)/)
                let score = -1
                let bareme = 1
                if (extract_score) {
                    score = parseFloat(extract_score[1])
                    bareme = parseFloat(extract_score[2])
                }
                if (eleves[login] === undefined) {
                    eleves[login] = {responses:[]}
                }
                eleves[login].nom = nom
                eleves[login].prenom = prenom
                if (eleves[login][exercise_id] === undefined) {
                    eleves[login][exercise_id] = {score_maxi: 0, duree: 0, nb_essais: 0}
                }
                eleves[login][exercise_id].duree += duree
                eleves[login][exercise_id].nb_essais += 1
                if ((score / bareme) > eleves[login][exercise_id].score_maxi) {
                    eleves[login][exercise_id].score_maxi = score / bareme
                }
                let url_detail = `${urlBase}/main/exercise/`+resultat.cell[10].match(/(exercise_show.php\?[^'"]+)/)[1]
                $.get(url_detail, page => {
                    let retour=extract_response_student($(page),cadre)
                    eleves[login].responses.push(retour)
                    nb_rows++
                    if (nb_rows == res.rows.length){
                        genere_xls(cadre,eleves,bareme)
                        $("#gensynth").text("Genere synthese")
                        $("#gensynth").click(genere_csv)

                    }
                })
            })
        }
    })
}


(function() {
    'use strict';
    const b_genere=$("<button id='gensynth'>Genere xls pour la correction manuelle</button>")
    b_genere.click(genere_csv)
    $("body").append(b_genere)
})();
