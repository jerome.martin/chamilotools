from setuptools import setup, find_packages  # Always prefer setuptools over distutils
from codecs import open  # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='chamilotools',

    # useful: python setup.py sdist bdist_wheel upload
    version='0.6.0',

    description='Chamilo tools is a set of tools to interact with a Chamilo server without using your browser.',

    url='https://gitlab.com/chamilotools/chamilotools',

    license='CeCILL',

    classifiers=[
        'License :: CeCILL-B Free Software License Agreement (CECILL-B)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],

    packages=['chamilolib'],

    # List run-time dependencies here.  These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[
        'mediawiki',
        'xlrd',
        'xlwt',
        'bs4',
        'pyyaml==4.2b4',
        'markdown==2.6.11',
        'requests',
        'six',
        'beautifulsoup4==4.6.0'
    ],
)
