PY = ${shell git ls-files '*.py' | grep -v '^deps/'} chamilotools

USER =
COURSE =
INSTANCE =
OPTIONS =
CONFIG = config.mk
-include $(CONFIG)

.PHONY: static test check interactive quick
quick: static test
.NOTPARALLEL: check
check: quick interactive

static:
	for f in $(PY); do \
		python3 -m py_compile $$f || { echo; echo $$f; status=1; } \
	done; exit $$status
	pep8 $(PY) --max-line-length=125 --ignore=E402
	pyflakes $(PY)

test:
	./test/testCreateQuizChamilo.py

interactive:
	env USER=$(USER) COURSE=$(COURSE) INSTANCE=$(INSTANCE) OPTIONS="$(OPTIONS)" \
		./test/interactive_tests.sh
