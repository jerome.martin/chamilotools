# Yaml Syntax for Quizz #

## Yaml mini-tutorial ##

[http://www.yaml.org/](Yaml) is a markup language conceptually similar
to XML, but designed to be easier to read and write by human. The two
main concepts of Yaml are dictionaries an lists.

A dictionary associates a values to keys. For example, see the first
two lines of the above snippet:

```Yaml
title: Python & Computer science basics
syntax: mediawiki
```

This means "the title is 'Python & Computer science basics' and the
syntax is 'mediawiki'".

Lists are written as bullet lists (using `-` to introduce a new item).
In the example above, a list of questions is associated to the key
`questions`. The first question is of type `E`, the second of type
`T`, and so on.

Characters like `:`, `'` and `"` are special in Yaml, if you need to
use them literally, you can escape them by surrounding them with
quotes. For example, this uses double-quotes to escape the `'`:

```Yaml
title: "It isn't a problem to have quotes in title"
```

To write multi-line text, the `|` shortcut (called
[Block scalar indicator](http://yaml.org/spec/current.html#id2537677))
is convenient: type `|` at the end of a line and write text below. In
the example below, the value associated to `answers` is the multi-line
Python program (initial indentation removed).

```Yaml
  answers: |
    a = "Bonjour"
    n = len(a)
    b = ""
    while n > 0%:%
        b = b + a[n-1]
        n = n - 1
```

One can also write multi-line text with `"..."`, including newlines
between the opening `"` and the closing one, but then
*Yaml will strip the newlines*, so if lines are important, use this `|` syntax.

## Chamilotools Yaml Syntax ##

### Toplevel ###

Each quiz is defined by a Yaml file. The file starts with settings
that are global to the whole quiz:

- `title`: The title of the quizz

- `syntax`: The syntax to use in question texts (it can be overridden
   on a per-question basis, see below). The default is `html`. See
   [syntax.md](syntax.md) for possible values and details.

* `description`: An introductory text for the quizz (a possibly long
  complement to the title).

* `category` (optional): Category of question, can be used to build quiz by category.

* `attempts`: Number of attempts allowed.

* `feedbackFinal`: Feedback given to the user when completing the
  Quizz

* `score`: Default score for each question. Defaults to 1.

* `scoreNeg`: Default score for wrong answer. Default to -`score` for M type, 0 for others. Valid for C and M type.

* `questions`: The list of questions, see below.

### Questions ###

To start the list of questions, write `answers:` followed by a bullet
list. Each question looks like:

```Yaml
- type: E
  title: This is an example question
  description: "What is the correct answer?"
  answers:
    - ko: not this one
    - ko: not this one either
    - ok: This is the right one
    - ko: and this is not
```

Fields common to all questions are:

* `type`: The type of question (multiple-choice, text with blanks to
  fill-in, free text, ...). See [question-types.md](question-types.md).

* `title`: A plain text (no formatting allowed) giving the question's
  title.

* `category` (optional): Category of question, can be used to build quiz by category.

* `description`: A possibly formatted (see [syntax.md](syntax.md))
  text giving the question itself.

* `answers`: Possible answers. The format of this field depends on the
  types of questions (again, see [question-types.md](question-types.md)).

* `syntax` (optional): Syntax to use for the description and answers.
  Overrides the global `syntax` for this particular question. See
  [syntax.md](syntax.md).

* `syntaxAnswer` (optional): similar to `syntax`, but applies only to
  the answer. For example, you may set `syntax: mediawiki` and
  `syntaxAnswer: pre` to get advanced formatting for the question and
  provide pieces of code as answers.

* `score` (optional): The score for this question (positive integer).

* `scoreNeg`(optional): The score for one of wrong answer. 
