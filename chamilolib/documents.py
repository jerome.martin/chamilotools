# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import re

from chamilolib.create_quiz_lib import (
    connectOrResetChamilo,
)

from chamilolib.utilities import (
    LoadError,
    extract_confirmation
)


def upload_zip(filename, o, br=None):
    return upload_file(filename, o, br, unzip_file=True)


def upload_file(filename, o, br=None, unzip_file=False):
    br = connectOrResetChamilo(br, o)
    br.follow_link(url_regex=r"(^|/)document/document\.php.*cidReq=" +
                   o.courseName)
    br.follow_link(url_regex=r"(^|/)upload\.php")
    f = br.select_form("form#upload")
    unzip = f.form.find("input", {'name': 'unzip'})
    assert(unzip)
    if unzip_file:
        unzip.attrs['checked'] = '1'
    br['if_exists'] = 'overwrite'
    file = f.form.find("input", {'name': 'file'})
    assert(file)
    file.attrs["value"] = filename
    response = br.submit_selected(btnName="submitDocument")

    # Extract the answer (first div inside main_content).
    confirmation = response.soup.find("div", {"class": "alert"})
    if not confirmation:
        confirmation = response.soup.find(id="main_content").find("div")
    print(confirmation.getText())


def get_file_id(filename, br, o):
    id = None
    all_links_to_docs = br.get_current_page().find_all('a')
    pattern = (
        re.escape(o.instance.get_url_course(o.courseName)) +
        "/*" + "document/" +
        re.escape(filename) + '\?')
    # The HTML for files looks like
    # <tr>
    # <td><input name="ids[]" value="37" type="checkbox"></td>
    # <td>...<a href="http://chamilo/course/documents/<filename>?...">
    # </tr>
    # => find the '<a>', move up and find the corresponding <input> to get the id.
    for l in all_links_to_docs:
        if 'href' in l.attrs:
            href = l.attrs['href']
            if re.match(pattern, href):
                current = l
                while current.name != 'tr':
                    current = current.parent
                box = current.find('input', {'type': 'checkbox',
                                             'name': 'ids[]'})
                new_id = box.attrs['value']
                if id and id != new_id:
                    print("ERROR: Several links with different ids")
                    raise LoadError()
                id = new_id
    # The HTML for directories looks like
    # <tr>
    # <td><input name="ids[]" value="37" type="checkbox"></td>
    # <td>...<img alt="filename" title="filename" src=".../folder_document.gif">
    # </tr>
    # => find the '<img>', move up and find the corresponding <input> to get the id.
    all_img = br.get_current_page().find_all('img')
    for img in all_img:
        if 'title' in img.attrs and 'alt' in img.attrs:
            if (
                    img.attrs['title'] == filename and
                    img.attrs['alt'] == filename and
                    re.match('.*/folder_document.[a-zA-Z]+$', img.attrs['src'])
            ):
                current = img
                while current.name != 'tr':
                    current = current.parent
                box = current.find('input', {'type': 'checkbox',
                                             'name': 'ids[]'})
                new_id = box.attrs['value']
                if id and id != new_id:
                    print("ERROR: Several links with different ids")
                    raise LoadError()
                id = new_id
    if id is not None:
        return id

    if not o.fuzzy_file_name:
        return None

    links_to_doc = br.get_current_page().find_all('a', {'title': filename})
    for l in links_to_doc:
        new_id = int(re.match('.*id=([0-9]+)$', l.attrs['href']).group(1))
        if id and id != new_id:
            print("ERROR: Several links with different ids")
            raise LoadError()
        id = new_id
    return id


def delete_doc(o, br=None):
    br = connectOrResetChamilo(br, o)
    br.follow_link(url_regex=r"(^|/)document/document\.php.*cidReq=" +
                   o.courseName)
    br.select_form('#form_teacher_table_id')
    id = get_file_id(o.filename, br, o)
    br.reattach_tag_to_form('input', {'name': 'curdirpath'})
    br.reattach_tag_to_form('input', {'name': 'action'})
    br.reattach_tag_to_form('input', {'name': 'cidReq'})
    br.reattach_tag_to_form('input', {'name': 'ids[]'})
    if id is None:
        print("ERROR: Could not find file", o.filename)
        raise LoadError()
    br['ids[]'] = id
    br['action'] = 'delete'
    print("Deleting file", id)
    resp = br.submit_selected()
    print(extract_confirmation(resp).getText())


def get_doc(o, br=None):
    br = connectOrResetChamilo(br, o)
    resp = br.open(o.instance.get_url_course(o.courseName) + 'document/' + o.filename)
    return resp.content


def cmd_upload_zip(o):
    o.read_password()
    upload_zip(o.zip_file, o)


def cmd_upload_file(o):
    o.read_password()
    upload_file(o.file, o)


def cmd_delete_doc(o):
    o.read_password()
    delete_doc(o)


def cmd_get_doc(o):
    o.read_password()
    out = get_doc(o)
    if o.output_file is None:
        print(out, end='')
    else:
        f = open(o.output_file, 'wb')
        f.write(out)
        print("File downloaded to", o.output_file)
