#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from chamilolib.utilities import (
    ensure_str,
    error,
    LoadError
)


def is_multiple_choice(t):
    return t in ('E', 'C', 'G', 'M', 'MVF', 'MVFE', 'U')


def needs_answer(t):
    return t != 'O'


def yaml2quiz_answer(answer):
    if 'ok' in answer:
        a = {'text': ensure_str(answer['ok']),
             'correct': True}
    else:
        a = {'text': ensure_str(answer['ko']),
             'correct': False}
    if 'feedback' in answer:
        a['feedback'] = ensure_str(answer['feedback'])
    return a


def quiz2yaml_answer(answer):
    res = dict(answer)
    if res["correct"]:
        res["ok"] = res["text"]
    else:
        res["ko"] = res["text"]
    del res["correct"]
    del res["text"]
    return res


def yaml2quiz_A_answer(answer):
    if isinstance(answer, dict):
        k, v = list(answer.keys()), list(answer.values())
        assert len(k) == 1  # TODO: friendly message
        return {"text": str(k[0]), "correct": str(v[0])}
    if isinstance(answer, str):
        return {"text": None, "correct": answer}
    error(f'Badly formed answer: {answer}')


def quiz2yaml_A_answer(answer):
    t, c = answer["text"], answer["correct"]
    if t is None:
        return c
    else:
        return {t: c}


def dump_yaml(x):
    from yaml import safe_dump
    return safe_dump(x, default_flow_style=False,
                     encoding='utf-8', allow_unicode=True).decode('utf-8')


def pretty_yaml(x):
    if isinstance(x, str):
        return x
    else:
        return dump_yaml(x)


def check_type(fieldname, value, expected):
    if not isinstance(value, expected):
        error("'%s' field must be a %s, got a %s instead:\n%s" % (
              fieldname, expected.__name__, type(value).__name__,
              pretty_yaml(value)))


def yaml2quiz_question(question):
    res = dict(question)
    if 'type' not in res:
        res['type'] = 'C'  # default value
    t = res['type']
    if not needs_answer(t) and 'answers' not in res:
        # Most of the code is generic and expects an 'answer' field.
        # Avoid special-casing everywhere and provide a dummy empty
        # answer.
        res['answers'] = ""
    if 'answers' not in res:
        if 'answer' in res:
            error("misspelling of 'answers' as 'answer':\n%s" %
                  dump_yaml(res))
        else:
            error("question has no 'answers' field:\n%s" %
                  dump_yaml(res))
    answers = res['answers']
    if is_multiple_choice(t):
        check_type('answers', answers, list)
        res['answers'] = [yaml2quiz_answer(x) for x in res['answers']]
    if t == 'A':
        check_type('answers', answers, list)
        res['answers'] = [yaml2quiz_A_answer(x) for x in res['answers']]
    return res


def quiz2yaml_question(question):
    res = dict(question)
    t = res['type']
    if is_multiple_choice(t):
        res['answers'] = [quiz2yaml_answer(x) for x in res['answers']]
    if t == 'A':
        res['answers'] = [quiz2yaml_A_answer(x) for x in res['answers']]
    return res


def quiz2yaml(d, a):
    res = dict(d)
    res['questions'] = [quiz2yaml_question(x) for x in a]
    return res


def yaml2quiz(q):
    d = dict(q)
    a = [yaml2quiz_question(x) for x in d['questions']]
    del d['questions']
    return d, a


def openQuizYaml(path):
    from yaml import safe_load, parser, scanner
    try:
        with open(path, 'r', encoding='utf8') as fd:
            yamlQuiz = safe_load(fd)
        return yaml2quiz(yamlQuiz)
    except (parser.ParserError, scanner.ScannerError) as e:
        print(e)
        raise LoadError
