#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from chamilolib.ChamiloInstance import (
    ChamiloInstance,
    # MIMETEX
)
from chamilolib.utilities import (
    display_html,
    AuthenticationError
)


class SimsuChamiloInstanceExt(ChamiloInstance):
    def connect(self, o):
        br = self.create_browser(o)
        br.open(self.get_url_base() + "/index.php")
        login_form = br.select_form('#formLogin')
        login_form.input({
            "login": o.username,
            "password": o.password})
        response = br.submit_selected()
        out = response.soup.encode()
        out_str = out.decode('utf8')
        if ("Invalid credentials" in out_str or
                "Mauvais identifiant / mot de passe." in out_str):
            display_html(response.soup, "Invalid login or password")
            raise AuthenticationError()
        return br

    def get_entry_url_course(self, course):
        return self.get_url_base() + "/main/auth/gotocourse.php?firstpage=" + course

#    def get_math_engine(self):
#       """Return the type of engine used to render Math."""
#        return MIMETEX
#    def has_double_escaping_bug(self):
#        return True
    def text_field_width(self, nb_chars):
        return nb_chars * 10

    def get_url_mimetex(self):
        return "http://latex.codecogs.com/gif.latex"
