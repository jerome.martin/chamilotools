#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import print_function

import datetime
import re
import sys
import webbrowser
import copy
import traceback
from pytz import timezone
import yaml

from chamilolib.cli import (
    CLIOptions,
    CLIError
)
from chamilolib.utilities import (
    LoadError,
    encode,
    error,
    warn
)
from chamilolib.xl_quiz import (
    openQuizXls
)
from chamilolib.yaml_quiz import (
    openQuizYaml,
    quiz2yaml,
    pretty_yaml,
    dump_yaml
)
import chamilolib.syntax

from mechanicalsoup import LinkNotFoundError
from chamilolib.create_quiz_lib import (
    connectOrResetChamilo,
)


def connectChamilo(o):
    br = o.instance.connect(o)
    br.instance = o.instance
    try:
        br.open(br.instance.get_url_work(o.courseName))
        resetChamilo(br, o.courseName)
    except LinkNotFoundError as e:
        raise CLIError("Invalid course name: %s" % (o.courseName,))
    return br


def resetChamilo(br, courseName):
    return br.open(br.instance.get_url_work(courseName))


def __create_work(o, br, name, ends_on=None, expires_on=None, description=None):
    resetChamilo(br, o.courseName)
    br.follow_link(url_regex=r"(^|/)work\.php\?.*cidReq=" +
                             o.courseName +
                             ".*&action=create_dir"
                   )
    br.set_debug(True)
    _form = br.select_form('form')
    # x = _form.form.select("input")
    # print(x)
    br["new_dir"] = name
    if description:
        br["description"] = description

    if expires_on is not None:
        expires_on_dt = datetime.datetime.strptime(expires_on, "%Y-%m-%d %H:%M")
        expires_on_str = expires_on_dt.strftime("%Y-%m-%d %H:%M")
        br["expires_on"] = expires_on_str
        br["enableExpiryDate"] = "1"
        if ends_on is not None:
            ends_on_dt = datetime.datetime.strptime(ends_on, "%Y-%m-%d %H:%M")
            if expires_on_dt >= ends_on_dt:
                print(f"ERREUR pour dossier '{name}' : expires_on_dt ({expires_on}) < ends_on_dt ({ends_on})")
                return
            ends_on_str = ends_on_dt.strftime("%Y-%m-%d %H:%M")
            br["ends_on"] = ends_on_str
            br["enableEndDate"] = "1"
    elif ends_on is None:
        print("Impossible d'avoir ends_on sans expires_on")

    res = br.submit_selected()
    if "La date de fin ne peut pas être antérieure à la date d'expiration" in res.text:
        print(f"ERREUR pour dossier '{name}' : La date de fin ne peut pas être antérieure à la date d'expiration")
    elif "Dossier créé" in res.text:
        print(f"OK : Dossier '{name}' créé")



def cmd_create_work(o, br=None):
    o.read_password()
    br = connectOrResetChamilo(br, o)
    __create_work(o, br, o.name, o.ends_on, o.expires_on, None)


def cmd_create_works(o, br=None):
    o.read_password()
    br = connectOrResetChamilo(br, o)
    f = o.file
    with open(o.file, encoding="utf-8") as fh:
        works = yaml.load(fh, Loader=yaml.SafeLoader)
    for work in works:
        __create_work(o, br, work["name"], work.get("ends_on"), work.get("expires_on"), work.get("description"))
    print(f)
