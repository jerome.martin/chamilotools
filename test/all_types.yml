title: Python & Computer science basics
description: This is an example quizz, whose goal is to illustrate ''all'' possible types supported by Chamilotools.
syntax: mediawiki
questions:
- type: C
  score: 1
  title: "Fractions (C: Multiple choice)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how the question is identical to the U-type question.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: M
  score: 1
  title: "Fractions (M: Multiple answers)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: T
  score: 8
  title: "Completez le programme suivant (T: Fill blanks or form)"
  syntax: pre
  separator: '$'
  answers: |
    a = "Hello"
    n = len(a)
    b = ""
    while n > 0$:$
        b = b + a[n-1]
        n = n - 1
  feedbackTrue: Good!
  feedbackFalse: Hey, you should know this ...
- type: A
  score: 1
  title: "A bit of history (A: Matching)"
  description: Associate events to dates
  answers:
    - 1623: Mechanical machine able to perform computation
    - 1939: First computer
    - 1975: Creation of Microsoft
    - 1974: Integrated circuits
    - 1976: Creation of Apple
    - 1981: Personal Computer
    - 1990: "Internet and hypertext links: the web" # quotes needed because of the : in the answer.
    - 1998: Creation of Google
    - 2004: Creation of Facebook
    - 2008: The most powerful computer reaches 1 PFlops (10 to the power 15 floating-point operation per seconds)
- type: O
  score: 1
  title: "This is an open question (O: Open question)"
  description: Type whatever you want in the box below, the teacher will decide whether it is the correct answer.
- type: E
  score: 1
  title: "Fractions (E: Exact Selection)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: U
  score: 1
  title: "Fractions (U: Unique answer with unknown)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how the question is identical to the C-type question.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: MVF
  score: 1
  title: "Fractions (MVF: Multiple answer true/false/don't know)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. This question is almost identical to the M-type question except the type, but adds scoreCorrect, scoreIncorrect and scoreDontKnow optional fields.
  scoreCorrect: 3
  scoreIncorrect: -2
  scoreDontKnow: -1
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: MVFE
  score: 1
  title: "Fractions (MVFE: Combination true/false/don't-know)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: G
  score: 1
  title: "Fractions (G: Global multiple answer)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
- type: G
  score: 1
  title: "Fractions (G: Global multiple answer, noNegative)"
  noNegative: true
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. This one has noNegative active, hence ticking everything gives you the maximum score.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
