#!/usr/bin/env python3


import sys
import os
TEST_DIR = os.path.dirname(__file__)
SCRIPT_DIR = os.path.join(os.path.dirname(__file__), "..")
SCRIPT = os.path.join(SCRIPT_DIR, "chamilotools")
sys.path.append(SCRIPT_DIR)

from chamilolib import syntax

from chamilolib.create_quiz_lib import (
    openQuiz,
    quiz2yaml
)
from chamilolib.yaml_quiz import (
    yaml2quiz
)

from chamilolib.cli import (
    CLIOptions
)
from chamilolib.CampusChamiloInstance import CampusChamiloInstance
from chamilolib.GrenobleINPChamiloInstance import GrenobleINPChamiloInstance
from chamilolib.ChamiloInstance import ChamiloInstance
from testCreateDownloadQuiz import TEST_FILES


import unittest
import subprocess


try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')


class TestQuiz2Yaml2Quiz(unittest.TestCase):
    def runTest(self):
        for basename in TEST_FILES:
            file = os.path.join(os.path.dirname(__file__), basename)
            print("Testing %s ..." % file)
            d, a = openQuiz(file)
            y = quiz2yaml(d, a)
            D, A = yaml2quiz(y)
            self.assertEqual(D, d)
            self.assertEqual(A, a)


class TestYaml2Yaml(unittest.TestCase):
    def testCleanup(self):
        cmd = [SCRIPT, 'create-quiz', '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml', '--cleanup']
        out = subprocess.check_output(cmd, stderr=DEVNULL, text=True)
        self.assertEqual(out, """questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")

    def testDump(self):
        cmd = [SCRIPT, 'create-quiz', '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml']
        out = subprocess.check_output(cmd, stderr=DEVNULL, text=True)
        self.assertEqual(out, """attempts: 0
description: ''
feedbackFinal: ''
questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  category: ''
  description: ''
  score: 1
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")


class TestCLI(unittest.TestCase):
    INSTANCE = GrenobleINPChamiloInstance()

    def testDefault(self):
        o = CLIOptions(['create-quiz'])
        self.assertEqual(o.username, '')
        self.assertEqual(o.password, '')
        self.assertEqual(o.courseName, '')
        self.assertEqual(o.filenames, [])
        self.assertEqual(o.wiki, False)
        self.assertEqual(o.feedback, True)
        self.assertEqual(o.nb, 0)
        self.assertEqual(o.mode, "2")
        self.assertEqual(o.passPercent, "0")
        self.assertEqual(o.timeLimit, "0")
        self.assertEqual(o.onePage, False)
        self.assertEqual(o.randomCategory, False)
        self.assertEqual(o.addMode, 'add')
        self.assertEqual(o.dumpYaml, False)
        self.assertEqual(o.debugMode, False)
        self.assertEqual(o.cleanUp, False)
        self.assertEqual(o.randomAnswers, False)
        self.assertEqual(o.view, False)
        self.assertEqual(o.hidden, False)
        self.assertEqual(o.urlBase, None)
        self.assertEqual(o.debug, False)
        self.assertEqual(o.verbose, 0)
        self.assertEqual(o.instance, None)
        self.assertEqual(o.instanceName, '')

    def testAll(self):
        o = CLIOptions("""
            create-quiz
            -d -w -u username -p 99
            -i i-in-file -i i-in-file2 file1 file2
            -c coursename -t 42 -l 12 -f -a -o -r -m 1
            --dump-yaml --cleanup
            --random-answers --view --hidden --url URL
            --debug --verbose --verbose
            """.split())
        self.assertEqual(o.username, 'username')
        self.assertEqual(o.password, '')
        self.assertEqual(o.courseName, 'coursename')
        self.assertEqual(o.filenames, ['i-in-file', 'i-in-file2', 'file1', 'file2'])
        self.assertEqual(o.wiki, True)
        self.assertEqual(o.feedback, False)
        self.assertEqual(o.nb, 42)
        self.assertEqual(o.mode, "0")
        self.assertEqual(o.passPercent, "99")
        self.assertEqual(o.timeLimit, "12")
        self.assertEqual(o.onePage, True)
        self.assertEqual(o.randomCategory, True)
        self.assertEqual(o.addMode, 'remove-same')
        self.assertEqual(o.dumpYaml, True)
        self.assertEqual(o.debugMode, True)
        self.assertEqual(o.cleanUp, True)
        self.assertEqual(o.randomAnswers, True)
        self.assertEqual(o.view, True)
        self.assertEqual(o.hidden, True)
        self.assertEqual(o.urlBase, 'URL')
        self.assertEqual(o.debug, True)
        self.assertEqual(o.verbose, 2)
        self.assertIsInstance(o.instance, ChamiloInstance)

    def testCampus(self):
        o = CLIOptions("create-quiz --campus".split())
        self.assertIsInstance(o.instance, CampusChamiloInstance)
        self.assertEqual(o.instanceName, 'Campus')

    def testInstance(self):
        o = CLIOptions("create-quiz --instance GrenobleINP".split())
        self.assertIsInstance(o.instance, GrenobleINPChamiloInstance)
        self.assertEqual(o.instanceName, 'GrenobleINP')

    def testMode(self):
        def test_one_mode(opt, val):
            o = CLIOptions(['create-quiz', opt])
            self.assertEqual(o.mode, val)
        test_one_mode('-a', "0")
        test_one_mode('-s', "1")
        test_one_mode('-e', "2")

    def testDownload(self):
        o = CLIOptions("""
            download-quiz
            --exercise-id 4242
            """.split())
        self.assertEqual(o.exerciseId, 4242)

    def testGlobal(self):
        o = CLIOptions("""-v -u some-user -c some-course --debug -d
            --url some-url --campus
            --password-from-stdin
            --instance Campus
            create-quiz
            -v -v
            """.split())
        self.assertEqual(o.username, 'some-user')
        self.assertEqual(o.courseName, 'some-course')
        self.assertEqual(o.debugMode, True)
        self.assertEqual(o.debug, True)
        self.assertEqual(o.urlBase, 'some-url')
        self.assertIsInstance(o.instance, CampusChamiloInstance)
        self.assertEqual(o.useCampus, True)
        self.assertEqual(o.passwordFromStdin, True)
        self.assertEqual(o.verbose, 3)

    def testGlobalOverride(self):
        o = CLIOptions("""-u some-user -c some-course --debug -d
            --url some-url
            --password-from-stdin
            --instance Campus
            create-quiz
            -u real-user -c real-course --debug -d --url real-url
            --instance GrenobleINP
            -v -v
            """.split())
        self.assertEqual(o.username, 'real-user')
        self.assertEqual(o.courseName, 'real-course')
        self.assertEqual(o.debugMode, True)
        self.assertEqual(o.debug, True)
        self.assertEqual(o.urlBase, 'real-url')
        self.assertEqual(o.instanceName, 'GrenobleINP')
        self.assertIsInstance(o.instance, GrenobleINPChamiloInstance)
        self.assertEqual(o.passwordFromStdin, True)
        self.assertEqual(o.verbose, 2)

    def testConvertDesc(self):
        i = """This text uses math mode: <math>\\frac{1}{x}</math>"""
        o = syntax.convert_desc(self.INSTANCE,
                                i, "mediawiki", "", "", "TESTCOURSE")
        # self.assertEqual(
        #    o,
        #    '<p>This text uses math mode: '
        #    '<img src="http://chamilo2.grenet.fr/cgi-bin/mimetex.cgi'
        #    '?%5Cfrac%7B1%7D%7Bx%7D" />\n</p>')
        self.assertEqual(
            o,
            '<p>This text uses math mode: '
            '<img src="http://latex.codecogs.com/gif.latex?%5Cfrac%7B1%7D%7Bx%7D" />\n</p>')


class TestCheck(unittest.TestCase):
    def __init__(self, *args):
        super(TestCheck, self).__init__(*args)
        if not hasattr(self, 'assertRegex'):
            # Python 2
            self.assertRegex = self.assertRegexpMatches

    def test_check_ok(self):
        cmd = [
            SCRIPT, 'check-quiz',
            os.path.join(TEST_DIR, 'qcm_python.yml'),
            os.path.join(TEST_DIR, 'quiz_for_test.xls')
        ]
        out = subprocess.check_output(cmd, stderr=DEVNULL, text=True).split('\n')
        self.assertRegex(out[0], 'Checking .*/qcm_python.yml ...')
        self.assertRegex(out[1], 'Checking .*/quiz_for_test.xls ...')

    def test_check_ko(self):
        ok = False
        try:
            cmd = [
                SCRIPT, 'check-quiz',
                os.path.join(TEST_DIR, 'err/yaml_syntax_error.yml')
            ]
            subprocess.check_output(cmd, stderr=DEVNULL, text=True)
        except subprocess.CalledProcessError:
            ok = True
        self.assertTrue(ok)


if __name__ == "__main__":
    unittest.main()
