#!/bin/sh

dir=$(cd "$(dirname "$0")"/../ && pwd)

die () {
    printf "%s\n" "$*"
    exit 1
}

sed "s/@DATE@/$(date)/" description.html > description-date.html
(
    echo "Dernière mise à jour le "
    date
) > docs/derniere-maj.txt
rm -f cours.zip && zip -r cours.zip docs/
$dir/setChamilo --user moym --coursId ENSIMAGTESTMOYM --documents cours.zip --intro description-date.html || die "setChamilo failed"

firefox http://chamilo2.grenet.fr/inp/courses/ENSIMAGTESTMOYM/index.php
